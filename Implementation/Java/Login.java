import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.imageio.*;

public class Login  extends JPanel {
    //public static ComfirmationWindow comfirmationWindow;
    //public static VerificationWindow verificationWindow;

    public Login(){

    JPanel panel = this;
    	
	this.setSize(500,500);
	this.setBackground(Color.PINK);
	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

	//Compte Facebook	
    JLabel courrielL = new JLabel("Entrer votre courriel");
    JTextField courrielT = new JTextField(20);
    courrielT.setMaximumSize(new Dimension(150,20));
    courrielL.setAlignmentX(Component.CENTER_ALIGNMENT);
    courrielT.setAlignmentX(Component.CENTER_ALIGNMENT);
	this.add(courrielL);
	this.add(courrielT);
	//Login Membre
	JButton loginB = new JButton("Login Membre");
	loginB.setAlignmentX(Component.CENTER_ALIGNMENT);

	loginB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			String courriel = courrielT.getText();
			Membre membre = Main.cd.comfirmerCourrielMembre(courriel);
			if(membre != null && !(courriel.equals(""))){
				if(membre.paye) {
					MainFrame.mainMenu = new MainMenu("membre", membre);
			 		MainFrame.frame.add(MainFrame.mainMenu);	
			 		MainFrame.mainMenu.setVisible(true);
			 		panel.setVisible(false);
				}else {
					courrielL.setText("Membre suspendu: frais sont dus");
				}
			}else {
				courrielL.setText("Le courriel n'est pas lie a un compte");
			}
		}
    });
	this.add(loginB)
	;
	//Login Pro
		JButton loginPB = new JButton("Login Professionnel");
		loginPB.setAlignmentX(Component.CENTER_ALIGNMENT);

		loginPB.addActionListener(new ActionListener()
		    {
			public void actionPerformed(ActionEvent e)
			{
				String courriel = courrielT.getText();
				Professionnel pro = Main.cd.comfirmerCourrielPro(courriel);
				if(pro != null && !(courriel.equals(""))){
					MainFrame.mainMenu = new MainMenu("pro", pro);
					MainFrame.frame.add(MainFrame.mainMenu);	
					MainFrame.mainMenu.setVisible(true);
					panel.setVisible(false);
				}else {
					courrielL.setText("Le courriel n'est pas lie a un compte");
				}

			}
	    });
		this.add(loginPB);
	
	//Login Agent
	JLabel agentL = new JLabel("(L'Agent n'a pas besoin de rentrer son courriel)");
	agentL.setAlignmentX(Component.CENTER_ALIGNMENT);
	this.add(agentL);
	JButton loginAB = new JButton("Login Agent");
	loginAB.setAlignmentX(Component.CENTER_ALIGNMENT);

	loginAB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
				MainFrame.mainMenu = new MainMenu("agent",null);	
			    MainFrame.frame.add(MainFrame.mainMenu);	
                MainFrame.mainMenu.setVisible(true);
                panel.setVisible(false);

		}
    });
	this.add(loginAB);
	
    }

    public void retourLogin(JPanel panel){
        panel.setVisible(false);
        MainFrame.frame.remove(panel);
        MainFrame.login.setVisible(true);
    }
}
