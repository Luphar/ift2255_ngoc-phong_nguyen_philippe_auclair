import java.awt.*;
import javax.swing.*;

public class MainFrame{
    //Frame
    public static JFrame frame;
    public static MainMenu mainMenu;
    public static Login login;

    public MainFrame(){
        MainFrame.frame = new JFrame("GYM1");

        frame.setBounds(0, 0, 500, 1000);
		frame.setBackground(Color.DARK_GRAY);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    	login = new Login();
            
    	MainFrame.frame.setLayout(null);
		MainFrame.frame.add(login);
		MainFrame.frame.setVisible(true);

    }
}
    
