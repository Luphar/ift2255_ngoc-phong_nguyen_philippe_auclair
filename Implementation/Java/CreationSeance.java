import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.logging.*;
import javax.imageio.*;
import java.time.*;
import javax.swing.text.*;

public class CreationSeance extends JPanel{

    public CreationSeance(Seance existante){
        String[] joursSemaine = {"Samedi","Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi"};
        Map<String,JCheckBox> boutonsJours = new HashMap<String,JCheckBox>();

    	JPanel panel = this;

        this.setSize(500,1000);
        this.setBackground(Color.RED);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JLabel titre = new JLabel("Creation Seance");
        if(existante != null){
            titre.setText("Modifier Seance");
        }
        titre.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(titre);

        //Date actuelle
        LocalDate date = java.time.LocalDate.now();
        //Temps actuel
        LocalTime time = java.time.LocalTime.now();

        JLabel dateAT = new JLabel("Date Actuelle: " + String.valueOf(date));
        JLabel timeAT = new JLabel("Heure Actuelle: " + String.valueOf(time));

        dateAT.setAlignmentX(Component.CENTER_ALIGNMENT);
        timeAT.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(dateAT);
        this.add(timeAT);

        JLabel dateDT = new JLabel("Date depart");
        JLabel dateFT = new JLabel("Date fin");

        JFormattedTextField dateDTT = new JFormattedTextField(createFormatter("##-##-####"));
        dateDTT.setAlignmentX(Component.CENTER_ALIGNMENT);
        dateDTT.setMaximumSize(new Dimension(100,20));

        JFormattedTextField dateFTT = new JFormattedTextField(createFormatter("##-##-####"));
        dateFTT.setAlignmentX(Component.CENTER_ALIGNMENT);
        dateFTT.setMaximumSize(new Dimension(100,20));

        dateDT.setAlignmentX(Component.CENTER_ALIGNMENT);
        dateFT.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(dateDT);
        this.add(dateDTT);
        this.add(dateFT);
        this.add(dateFTT);

        JLabel timeDT = new JLabel("Heure depart");
        JLabel timeFT = new JLabel("Heure fin");

        JFormattedTextField timeDTT = new JFormattedTextField(createFormatter("##:##"));
        timeDTT.setAlignmentX(Component.CENTER_ALIGNMENT);
        timeDTT.setMaximumSize(new Dimension(70,20));

        JFormattedTextField timeFTT = new JFormattedTextField(createFormatter("##:##"));
        timeFTT.setAlignmentX(Component.CENTER_ALIGNMENT);
        timeFTT.setMaximumSize(new Dimension(70,20));

        timeDT.setAlignmentX(Component.CENTER_ALIGNMENT);
        timeFT.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(timeDT);
        this.add(timeDTT);
        this.add(timeFT);
        this.add(timeFTT);
        
        //Type de service
        JLabel servicesL = new JLabel("Type de la seance");
        servicesL.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(servicesL);
        String[] listeService = Main.cd.getServices();
        JComboBox services = new JComboBox(listeService);
        services.setMaximumSize(new Dimension(200,20));
        this.add(services);
        
        //Recurrence hebdomadaire du service
        JLabel recurrenceL = new JLabel("Recurrence hebdomadaire du service");
        recurrenceL.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(recurrenceL);
        for(int i=0;i<joursSemaine.length;i++){
            boutonsJours.put(joursSemaine[i],new JCheckBox(joursSemaine[i]));
            add(boutonsJours.get(joursSemaine[i]));
        }
        //Capacite maximale
        JLabel capMaxT = new JLabel("Capacite Maximale");
        capMaxT.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(capMaxT);
        SpinnerModel valeurs = new SpinnerNumberModel(1,1,30,1);
        JSpinner capMax = new JSpinner(valeurs);
        capMax.setMaximumSize(new Dimension(50,25));

        capMax.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(capMax);

        //Prix du service
        JLabel prixT = new JLabel("Prix de la seance");
        prixT.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(prixT);
        SpinnerModel valeursPrix = new SpinnerNumberModel(0,0,100,0.01);
        JSpinner prixMax = new JSpinner(valeursPrix);
        prixMax.setMaximumSize(new Dimension(70,25));

        prixMax.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(prixMax);

        //numero du professionnel
        JLabel numeroProT = new JLabel("Numero Professionnel");
        numeroProT.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField numeroPro = new JTextField(20);
        numeroPro.setMaximumSize(new Dimension(1000,20));

        this.add(numeroProT);
        this.add(numeroPro);

        //Commentaire
        JLabel commentaireZoneT = new JLabel("Commentaire (facultatif)");
        commentaireZoneT.setAlignmentX(Component.CENTER_ALIGNMENT);

        TextArea commentaireZone = new TextArea("", 5, 40);
        commentaireZone.setMaximumSize(new Dimension(300,200));

        this.add(commentaireZoneT);
        this.add(commentaireZone);

        JLabel resultat = new JLabel("Appuyer sur comfirmer pour continuer");
        resultat.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(resultat);

        JButton confirmer = new JButton("Confirmer");
        confirmer.setAlignmentX(Component.CENTER_ALIGNMENT);

        this.add(confirmer);

        if(existante != null){
            dateDTT.setValue(existante.dateD);
            dateFTT.setValue(existante.dateF);
            timeDTT.setValue(existante.heureD);
            timeFTT.setValue(existante.heureF);
            for(int i=0;i<boutonsJours.size();i++){
                boutonsJours.get(joursSemaine[i]).setSelected(existante.jours[i]);
            }
            capMax.setValue(existante.capaciteMax);
            numeroPro.setText(existante.noPro);
            prixMax.setValue(existante.prix);

        }

        confirmer.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    if(Main.cd.getProParCode(numeroPro.getText()) != null){
                        String dateA = String.valueOf(date);
                        String heureA = String.valueOf(time);

                        String dateD = dateDTT.getText();
                        String dateF = dateFTT.getText();
                        
                        String type = services.getSelectedItem().toString();
                        
                        String heureD = timeDTT.getText();
                        String heureF = timeFTT.getText();

                        boolean[] jours = new boolean[7];

                        for(int i=0;i<boutonsJours.size();i++){
                            jours[i] = boutonsJours.get(joursSemaine[i]).isSelected();
                        }

                        Integer capMaxVal = (Integer) capMax.getValue();

                        String noPro = numeroPro.getText();
                        String commentaireT = commentaireZone.getText();

                        double prixS = (double)prixMax.getValue();


                        if(existante == null){

                            String code = Main.cd.addSeance(noPro, dateA,dateD, dateF,type,
                                                            heureA, heureD, heureF, jours,
                                                            capMaxVal, prixS,commentaireT);
                            resultat.setText( "Numero de Seance: " + code);
                        }else{
                            existante.dateD = dateD;
                            existante.dateF = dateF;
                            existante.heureD = heureD;
                            existante.heureF = heureF;
                            existante.jours = jours;
                            existante.capaciteMax = capMaxVal;
                        }

                    }else{
                        numeroProT.setText("Le professionnel n'existe pas");
                    }
                }
	    });
        JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);

        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    MainFrame.mainMenu.retour(panel);
		}
	    });
        this.add(retour);
    }
    //Cette methode est prise depuis l'internet http://www.java2s.com/Tutorials/Java/Swing_How_to/JFormattedTextField/Create_custom_format_for_Date_Time_value_in_Formattedtextfield.htm
    private MaskFormatter createFormatter(String s) {
        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
        }
        return formatter;
    }
};
