import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.imageio.*;

public class PersonneWindow extends JPanel{

    public PersonneWindow(String type, String nom){
    	JPanel panel = this;
    	System.out.println(type + " " + nom + "\n");
    	Personne personne;
    	
    	if(type.equals("membre")){
			personne = (Membre)Main.cd.getMembreParNom(nom);
		}else{
			personne = (Professionnel)Main.cd.getProParNom(nom);
		}

    	this.setSize(500,500);
    	this.setBackground(Color.YELLOW);
    	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    	//Numero de membre
		JLabel nomL = new JLabel("Code: " + personne.code);
		JLabel nomCL = new JLabel("Nom");
		JTextField nomT = new JTextField(25);
		nomT.setMaximumSize(new Dimension(150,20));
		nomT.setText(personne.nom);
		 //courriel
        JLabel courrielL = new JLabel("Courriel");
        courrielL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField courrielT = new JTextField(20);
        courrielT.setMaximumSize(new Dimension(150,20));
        courrielT.setText(personne.courriel);
        //Adresse
        JLabel adresseL = new JLabel("Adresse");
        adresseL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField adresseT = new JTextField(25);
        adresseT.setMaximumSize(new Dimension(150,20));
        adresseT.setText(personne.adresse);
        //Ville
        JLabel villeL = new JLabel("Ville");
        villeL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField villeT = new JTextField(14);
        villeT.setMaximumSize(new Dimension(150,20));
        villeT.setText(personne.ville);
        //Province
        JLabel provinceL = new JLabel("Province");
        provinceL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField provinceT = new JTextField(2);
        provinceT.setMaximumSize(new Dimension(150,20));
        provinceT.setText(personne.province);
        //Code postal
        JLabel postalL = new JLabel("Code postal");
        postalL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField postalT = new JTextField(6);
        postalT.setMaximumSize(new Dimension(150,20));
        postalT.setText(personne.codePostal);
        
		JButton comfirmer = new JButton("Comfirmer");
		comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);

		comfirmer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			personne.nom = nomT.getText();
			personne.courriel = courrielT.getText();
			personne.adresse = adresseT.getText();
			personne.ville = villeT.getText();
			personne.province = provinceT.getText();
			personne.codePostal = postalT.getText();
			MainFrame.mainMenu.retour(panel);
		}
	    });
		JLabel supprimerL = new JLabel("Supprimer: " + personne.nom + " du système");
	    JButton supprimer = new JButton("Supprimer");
	    supprimerL.setAlignmentX(Component.CENTER_ALIGNMENT);
		supprimer.setAlignmentX(Component.CENTER_ALIGNMENT);

		supprimer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			if(type.equals("membre")){
				Main.cd.getListeMembre().remove(Main.cd.getMembreParNom(nom));
			}else{
				Main.cd.getListePro().remove(Main.cd.getProParNom(nom));
			}
			MainFrame.mainMenu.retour(panel);
		}
	    });

	    JButton retour = new JButton("Retour");
		retour.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			MainFrame.mainMenu.retour(panel);
		}
	    });

		nomL.setAlignmentX(Component.CENTER_ALIGNMENT);
		nomCL.setAlignmentX(Component.CENTER_ALIGNMENT);
		nomT.setAlignmentX(Component.CENTER_ALIGNMENT);
		comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);
		retour.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(nomL);
		this.add(nomCL);
		this.add(nomT);
		this.add(courrielL);
        this.add(courrielT);
        this.add(adresseL);
        this.add(adresseT);
        this.add(villeL);
        this.add(villeT);
        this.add(provinceL);
        this.add(provinceT);
        this.add(postalL);
        this.add(postalT);
		this.add(comfirmer);
		if(!(type.equals("pro")) && !(type.equals("membre"))){
			this.add(supprimerL);
			this.add(supprimer);
		}
		this.add(retour);
	}
		
}
