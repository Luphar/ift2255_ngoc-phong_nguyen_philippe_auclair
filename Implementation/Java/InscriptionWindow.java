import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.imageio.*;
import java.time.*;

public class InscriptionWindow extends JPanel{

    Boolean payer = false;
    
    public InscriptionWindow(Seance seance,Personne personne, String type){

    	JPanel panel = this;

        this.setSize(500,600);
        this.setBackground(Color.RED);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        //Date actuelle
        LocalDate date = java.time.LocalDate.now();
        //Temps actuel
        LocalTime time = java.time.LocalTime.now();

        JLabel dateAT = new JLabel("Date Actuelle: " + String.valueOf(date));
        JLabel timeAT = new JLabel("Heure Actuelle: " + String.valueOf(time));

        dateAT.setAlignmentX(Component.CENTER_ALIGNMENT);
        timeAT.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(dateAT);
        this.add(timeAT);

        JLabel professionnel = new JLabel("Professionnel: " + seance.noPro);
        professionnel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(professionnel);

        //Numero de membre
        String message = "";
        if(type.equals("membre")) {
        	message ="Entrer le numero de membre";
        }else {
        	message = "Entrer le QR du membre";
        }
        JLabel noMembreL = new JLabel(message);
        JTextField noMembreT = new JTextField(9);
        if(type.equals("membre") || type.equals("pro")) {
        	noMembreT.setText(personne.code);
        }
        noMembreT.setMaximumSize(new Dimension(100,20));
        noMembreL.setAlignmentX(Component.CENTER_ALIGNMENT);
        noMembreT.setAlignmentX(Component.CENTER_ALIGNMENT);


        this.add(noMembreL);
        this.add(noMembreT);
        String messagePaie = String.format("Paiement %.2f $: Pas Encore Paye", seance.prix);
        
        JLabel paiementL = new JLabel(messagePaie);
        paiementL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton paiementB = new JButton("Payer");
        paiementB.setAlignmentX(Component.CENTER_ALIGNMENT);
        paiementB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    new Paiement();
                    paiementL.setText("Paiement: Accepte");
                    payer = true;
		}
	    });
        if(type.equals("membre")) {
        	this.add(paiementL);
        	this.add(paiementB);
        }
        //Commentaire
        JLabel commentaireZoneT = new JLabel("Commentaire (facultatif)");
        commentaireZoneT.setAlignmentX(Component.CENTER_ALIGNMENT);
        //Commentaire
        TextArea commentaireZone = new TextArea("", 5, 40);
        commentaireZone.setMaximumSize(new Dimension(300,200));
        if(type.equals("membre")) {
        	 this.add(commentaireZoneT);
             this.add(commentaireZone);	
        }

        JButton comfirmer = new JButton("Comfirmer");
        comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);
        if(type.equals("membre")) {
	        comfirmer.addActionListener(new ActionListener()
		    {
			public void actionPerformed(ActionEvent e)
			{
	                    String code = noMembreT.getText();
	                    if(Main.cd.contientCode(code)){
	                    	Membre membre = Main.cd.getMembreParCode(code);
	                        seance.membres.add(membre);
	                        Main.cd.ajouterInscription(seance, membre,date.toString(),time.toString(),commentaireZone.getText());
	                        noMembreL.setText("Inscription fini");
	                    }else{
	                        noMembreL.setText("Le membre n'existe pas");
	                    }
			}
		    });
        }else {
        	comfirmer.addActionListener(new ActionListener()
		    {
			public void actionPerformed(ActionEvent e)
			{
	                    String code = noMembreT.getText();
                    	if(Main.cd.comfirmerQRMembre(code)) {
                    		noMembreL.setText("Client Valide");
                    	}else{
                    		noMembreL.setText("Client Invalide");
                    	}
			}
		    });
        }
        this.add(comfirmer);

        JButton desinscrire = new JButton("desinscrire");
        desinscrire.setAlignmentX(Component.CENTER_ALIGNMENT);

        desinscrire.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    String code = noMembreT.getText();
                    if(Main.cd.getSeanceParCode(seance.code).membres.contains(Main.cd.getMembreParCode(code))){
                    	Main.cd.desinscrire(seance,Main.cd.getMembreParCode(code));
                        Main.cd.getSeanceParCode(seance.code).membres.remove(Main.cd.getMembreParCode(code));
                    }else{
                        noMembreL.setText("Le membre n'est pas inscrit a la seance");
                    }
		}
	    });
        if(type.equals("membre")) {
        	this.add(desinscrire);
        }
        JButton modifier = new JButton("modifier");
        modifier.setAlignmentX(Component.CENTER_ALIGNMENT);

        modifier.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    CreationSeance creationSWindow = new CreationSeance(seance);
                    MainFrame.frame.add(creationSWindow);
                    panel.setVisible(false);
                    creationSWindow.setVisible(true);
		}
	    });

        JButton supprimer = new JButton("Supprimer");
        supprimer.setAlignmentX(Component.CENTER_ALIGNMENT);

        supprimer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    Main.cd.getListeSeances().remove(seance);
                    MainFrame.mainMenu.retour(panel);
		}
	    });

        JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);

        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    MainFrame.mainMenu.retour(panel);
		}
	    });
        this.add(retour);
        if(!(type.equals("membre"))) {
        	this.add(supprimer);
            this.add(modifier);
        }
    }
};
