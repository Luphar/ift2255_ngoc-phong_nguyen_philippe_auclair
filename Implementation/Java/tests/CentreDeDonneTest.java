package tests;
import java.util.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CentreDeDonneTest {

	private CentreDeDonne cd = new CentreDeDonne();
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//#1 Teste la taille du code genere pour une personne
	@Test
	public void testGenerateCode() {
		String code = cd.generateCode("personne");
		assertEquals("code personne pas la bonne taille",9,code.length());
	}
	//#2 teste le code genere pour une seance
	@Test
	public void testGenerateCodeSeance() {
		 Map<String, String> codeService = new HashMap<String, String>() {{
	        	put("nutritionniste", "598");
	        	put("zumba", "883");
	    		}};
	    String noPro = "123456780";
	    String type = "nutritionniste";
		String code = cd.generateCodeSeance(noPro,type);
		assertEquals("code seance pas la bonne taille",7,code.length());
		String fincode = code.substring(5);
		String finpro = noPro.substring(7);
		assertTrue("probleme code seance, pro",fincode.equals(finpro));
		String debutcode = code.substring(0,3);
		assertTrue("probleme code seance,service",debutcode.equals(codeService.get("nutritionniste")));
	}
	private Seance seance;
	private Membre membre;
	@Before
	public void setUp() throws Exception {
		seance = new Seance("","","","","","","","","",new boolean[0],0, 0.0,"");
		membre = new Membre("Philippe","111111111","","","","","");
		membre.paye = true;
		seance.membres.add(membre);
		cd.getListeMembre().add(membre);
	}
	
	//#3 Teste si la methode ajoute un element dans la liste
	@Test
	public void testAjouterInscription() {
		cd.ajouterInscription(seance,membre,"","","");
		assertEquals("Taille liste inscription incorrecte",1,cd.listeInscription.size());
	}
	//4 Teste si la methode enleve un element dans la liste
	@Test
	public void testDesinscrire() {
		cd.desinscrire(seance, membre);
		assertEquals("Taille liste desinscrire incorrecte",0,cd.listeInscription.size());
	}
	//5 Teste si le QR retourne la bonne valeure
	@Test
	public void testComfirmerQRMembre() {
		cd.ajouterInscription(seance,membre,"","","");
		assertTrue("QR ne comfirme pas", cd.comfirmerQRMembre(membre.code));
		assertFalse("QR ne devrait pas comfirmer", cd.comfirmerQRMembre("1"));
		membre.paye = false;
		assertFalse("QR ne devrait pas valider si pas paye", cd.comfirmerQRMembre(membre.code));
	}
	//6 Tester les methodes de recherches pas code
	@Test
	public void testGetMembreParCode() {
		Membre membre = cd.getMembreParCode("111111111");
		assertTrue("Membre n'est pas trouve",membre.nom.equals("Philippe"));
		membre = cd.getMembreParCode("00000000");
		assertNull("Membre ne devrait pas etre trouve",membre);
	}
	/*
	@Test
	public void testGetProParNom() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetProParCode() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetSeanceParCode() {
		fail("Not yet implemented");
	}

	@Test
	public void testComfirmerCourrielMembre() {
		fail("Not yet implemented");
	}

	@Test
	public void testComfirmerCourrielPro() {
		fail("Not yet implemented");
	}*/


}
