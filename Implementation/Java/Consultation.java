import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.awt.Color;
import java.awt.Component;
import javax.swing.table.DefaultTableModel;

public class Consultation extends JPanel{

    public Consultation(Personne personne,String type){

        this.setSize(500,500);
        this.setBackground(Color.RED);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        
        JLabel description = new JLabel("Chercher toutes les seances par no de Professionnel");
        this.add(description);
        description.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField recherche = new JTextField(20);
        recherche.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton rechercheB = new JButton("rechercher");
        rechercheB.setAlignmentX(Component.CENTER_ALIGNMENT);

        this.add(recherche);
        this.add(rechercheB);
        String[] nomColonne;
        JPanel panel = this;
        if(type.equals("membre")){
        	nomColonne = new String[] {"Code","Type","Date","Heure","Prix"};
        }else {
        	nomColonne = new String[]{"Code","Type","Date","Heure","Capacite max"};
        }
        ArrayList<Seance> listeSeance = Main.cd.getListeSeances();
        Object[][] data = new Object[listeSeance.size()][5];

        //Trouve le jour de la semaine courrant
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        if(day == 7){day = 0;}//Converti la journe(Nous commencons par Samedi)
        
        for(int i=0;i< listeSeance.size();i++){
            Seance seance = listeSeance.get(i);
            if(type.equals("membre") && seance.jours[day]) {//Si le cours est offert ajd, pour un membre
            	Object[] range = {
            			seance.code,
            			seance.type,
            			seance.dateD + " à " + seance.dateF,
            			seance.heureD + " à " + seance.heureF,
            			seance.prix
            	};
            	data[i] = range;
            }else if(type.equals("pro") && seance.noPro.equals(personne.code)) {
             	Object[] range = {
            			seance.code,
            			seance.type,
            			seance.dateD + " à " + seance.dateF,
            			seance.heureD + " à " + seance.heureF,
            			seance.capaciteMax
            	};
             	data[i] = range;
    		}else {
            	Object[] range = {
            			seance.code,
            			seance.type,
            			seance.dateD + " à " + seance.dateF,
            			seance.heureD + " à " + seance.heureF,
            			seance.prix
            	};
            	data[i] = range;
    		}
        }
        DefaultTableModel model = new DefaultTableModel(data, nomColonne);
        JTable table = new JTable(model);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
    		public void mouseClicked(java.awt.event.MouseEvent evt) {
                    int row = table.rowAtPoint(evt.getPoint());
                    int col = table.columnAtPoint(evt.getPoint());
                    if (row >= 0 && col >= 0) {
                        InscriptionWindow inscriptionWindow = new InscriptionWindow(listeSeance.get(row),personne,type);
                        MainFrame.frame.add(inscriptionWindow);
                        panel.setVisible(false);
                        inscriptionWindow.setVisible(true);
                    }
    		}
            });
        this.add(new JScrollPane(table));

        rechercheB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    if(Main.cd.getProParCode(recherche.getText()) == null){
                        description.setText("Le Professionnel n'existe pas");
                    }

                    for(int i=0;i<model.getRowCount(); i++){
                        if(!(recherche.getText().equals(data[i][1]))){
                            model.removeRow(i);
                            i--;
                        }
                    }
		}
	    });

        JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);

        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    MainFrame.mainMenu.retour(panel);
		}
	    });
        this.add(retour);
    };
}
