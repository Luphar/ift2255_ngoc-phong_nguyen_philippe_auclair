import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.imageio.*;
import java.time.*;

public class Paiement{

    public JPanel parent;
    
    public Paiement(){

        JFrame frame = new JFrame("paiment");
        frame.setBounds(0, 0, 500, 500);
        frame.setBackground(Color.DARK_GRAY);
        frame.setResizable(false);
        JPanel panel = new JPanel();
        panel.setSize(500,500);
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        //frame.setDefaultCloseOperation(frame.dispose());

        String message = "Il s'agit d'une page de paiment,\n"
            + "en theorie devrait diriger vers une page de banque\n"
            + "l'utilisateur comfirme, et la page retourne un boolean pour savoir si le paiement est accepte";
        TextArea explication = new TextArea(message);
        boolean accepter = verificationPaiment();
        JLabel comfirmation = new JLabel();
        comfirmation.setAlignmentX(Component.CENTER_ALIGNMENT);
        if(accepter){
            comfirmation.setText("Le paiement est accepté");
        }else{
            comfirmation.setText("Le paiement est refusé");
        }

	JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);
        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    frame.dispose();
		}
	    });

	panel.add(explication);
        panel.add(comfirmation);
	panel.add(retour);
        frame.add(panel);
        frame.setVisible(true);
    }

    public boolean verificationPaiment(){
        return true;
    }

}
