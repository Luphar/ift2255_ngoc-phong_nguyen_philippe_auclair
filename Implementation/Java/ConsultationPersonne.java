import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.awt.Color;
import java.awt.Component;
import javax.swing.table.DefaultTableModel;

public class ConsultationPersonne extends JPanel{
    
    public ConsultationPersonne(){
        this.setSize(500,500);
        this.setBackground(Color.RED);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel description = new JLabel("Chercher toutes les seances par no de Professionnel");
        this.add(description);
        description.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField recherche = new JTextField(20);
        recherche.setAlignmentX(Component.CENTER_ALIGNMENT);

        JButton rechercheB = new JButton("rechercher");
        rechercheB.setAlignmentX(Component.CENTER_ALIGNMENT);

        this.add(recherche);
        this.add(rechercheB);

        JPanel panel = this;

        String[] nomColonne = {
            "Code",
            "Type",
            "Nom",
            "Courriel"
        };
        ArrayList<Membre> listeMembre = Main.cd.getListeMembre();
        ArrayList<Professionnel> listePro = Main.cd.getListePro();

        Object[][] data = new Object[listeMembre.size() + listePro.size()][4]; 

        int index = 0;

        for(int i=0;i< listeMembre.size();i++){
            Membre personne = listeMembre.get(i);
            String type = "Membre";
            Object[] range = {
                personne.code,
                type,
                personne.nom,
                personne.courriel
            };
            data[i] = range;
            index++;
        }
        for(int i=0;i<listePro.size();i++){
            Professionnel personne = listePro.get(i);
            String type = "Professionnel";
            Object[] range = {
                personne.code,
                type,
                personne.nom,
                personne.courriel
            };
            data[i + index] = range;
        }
        DefaultTableModel model = new DefaultTableModel(data, nomColonne);
		
        JTable table = new JTable(model);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
    		public void mouseClicked(java.awt.event.MouseEvent evt) {
                    int row = table.rowAtPoint(evt.getPoint());
                    int col = table.columnAtPoint(evt.getPoint());
                    if (row >= 0 && col >= 0) {
                        PersonneWindow personneWindow;
                        if(data[row][1].equals("Membre")){
                            personneWindow = new PersonneWindow("membre", listeMembre.get(row).nom);
                        }else{
                            personneWindow = new PersonneWindow("professionnel",listePro.get(row - listeMembre.size()).nom);
                        }
                        MainFrame.frame.add(personneWindow);
                        panel.setVisible(false);
                        personneWindow.setVisible(true);
                    }
    		}
            });
        this.add(new JScrollPane(table));

        rechercheB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    for(int i=0;i<model.getRowCount(); i++){
                        if(!(recherche.getText().equals(data[i][0]))){
                            model.removeRow(i);
                            i--;
                        }			
                    }
		}
	    });

        JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);
		
        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    MainFrame.mainMenu.retour(panel);
		}
	    });
        this.add(retour);
    };
}
