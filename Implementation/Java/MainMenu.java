import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.imageio.*;

public class MainMenu  extends JPanel {
    //public static ComfirmationWindow comfirmationWindow;
    //public static VerificationWindow verificationWindow;

    public MainMenu(String type, Personne personne){

    JPanel panel = this;
    	
	this.setSize(500,500);
	this.setBackground(Color.ORANGE);
	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	//Membre
	JLabel membreL = new JLabel("#Membre");
	membreL.setFont(new Font("Verdana", Font.PLAIN, 18));
	membreL.setAlignmentX(Component.CENTER_ALIGNMENT);

	//Vers la page de creation de membre
	JLabel membreCL = new JLabel("Creer un nouveau membre");
	JButton boutonCre = new JButton("Creer Membre");
	boutonCre.setAlignmentX(Component.CENTER_ALIGNMENT);
	membreCL.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonCre.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
		    CreationMembreWindow creationMembreWindow = new CreationMembreWindow();
                    MainFrame.frame.add(creationMembreWindow);
                    MainFrame.mainMenu.setVisible(false);
                    creationMembreWindow.setVisible(true);
		}
	    });

	//Vers la page de verification
	JLabel membreVL = new JLabel("Valider un numero de membre");
	JButton boutonVer= new JButton("Validation");
	boutonVer.setAlignmentX(Component.CENTER_ALIGNMENT);
	membreVL.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonVer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
		    VerificationWindow verificationWindow = new VerificationWindow();
                    MainFrame.frame.add(verificationWindow);
                    MainFrame.mainMenu.setVisible(false);
                    verificationWindow.setVisible(true);
		}
	    });

	//Comfirmation qr
	JLabel membreCoL = new JLabel("Comfirmer le QR");
	JTextField membreCoT = new JTextField(20);
	membreCoT.setMaximumSize(new Dimension(150,20));
	JButton boutonComfirmation = new JButton("Valider");
	boutonComfirmation.setAlignmentX(Component.CENTER_ALIGNMENT);
	membreCoL.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonComfirmation.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			if(Main.cd.comfirmerQRMembre(membreCoT.getText())) {
				membreCoL.setText("Client Valide");
			}else {
				membreCoL.setText("Client Invalide");
			}
		}
        });
	//Professionnel
	JLabel professionnel = new JLabel("#Professionnel");
	professionnel.setFont(new Font("Verdana", Font.PLAIN, 18));
	professionnel.setAlignmentX(Component.CENTER_ALIGNMENT);
	//Cree nouveau pro
	JLabel creationPL = new JLabel("Cree un nouveau Professionnel");
	JButton boutonCreationP = new JButton("Creer Professionnel");
	creationPL.setAlignmentX(Component.CENTER_ALIGNMENT);
	boutonCreationP.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonCreationP.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    CreationPro creationPWindow = new CreationPro();
                    MainFrame.frame.add(creationPWindow);
                    MainFrame.mainMenu.setVisible(false);
                    creationPWindow.setVisible(true);

		}
        });
	//Creation Seance
	JLabel creationSL = new JLabel("Cree une sceance");
	JButton boutonCreationSL = new JButton("Creer Sceance");

	creationSL.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonCreationSL.setAlignmentX(Component.CENTER_ALIGNMENT);
	boutonCreationSL.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    CreationSeance creationSWindow = new CreationSeance(null);
                    MainFrame.frame.add(creationSWindow);
                    MainFrame.mainMenu.setVisible(false);
                    creationSWindow.setVisible(true);

		}
        });

	//Agent
	JLabel agentL = new JLabel("#Agent");
	agentL.setFont(new Font("Verdana", Font.PLAIN, 18));
	agentL.setAlignmentX(Component.CENTER_ALIGNMENT);
	
	//Consultation des seances
	JLabel consultationL = new JLabel("Consulter les sceances");
	JButton consultationB = new JButton("Consultation Seances");
	consultationL.setAlignmentX(Component.CENTER_ALIGNMENT);
	consultationB.setAlignmentX(Component.CENTER_ALIGNMENT);

	consultationB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    Consultation consultationWindow = new Consultation(personne,type);
                    MainFrame.frame.add(consultationWindow);
                    MainFrame.mainMenu.setVisible(false);
                    consultationWindow.setVisible(true);

		}
        });
	//Consultation des Personnes
	JLabel consultationPL = new JLabel("Consulter les Membres/Professionnels");
	JButton consultationPB = new JButton("Consultation Personne");
	consultationPL.setAlignmentX(Component.CENTER_ALIGNMENT);
	consultationPB.setAlignmentX(Component.CENTER_ALIGNMENT);

	consultationPB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    ConsultationPersonne consultationPerWindow = new ConsultationPersonne();
                    MainFrame.frame.add(consultationPerWindow);
                    MainFrame.mainMenu.setVisible(false);
                    consultationPerWindow.setVisible(true);

		}
        });
	//Modifier informations
		JLabel modL = new JLabel("Modifier les informations personnelles");
		JButton modB = new JButton("Modifier");
		modL.setAlignmentX(Component.CENTER_ALIGNMENT);
		modB.setAlignmentX(Component.CENTER_ALIGNMENT);

		modB.addActionListener(new ActionListener()
		    {
			public void actionPerformed(ActionEvent e)
			{
	                    PersonneWindow modW = new PersonneWindow(type,personne.nom);
	                    MainFrame.frame.add(modW);
	                    MainFrame.mainMenu.setVisible(false);
	                    modW.setVisible(true);

			}
	        });
	//Gerant
    JLabel gerantL = new JLabel("#Gerant");
	gerantL.setFont(new Font("Verdana", Font.PLAIN, 18));
        gerantL.setAlignmentX(Component.CENTER_ALIGNMENT);
        
    JLabel rapportL = new JLabel("Imprimer le rapport");
	JButton boutonRapport = new JButton("Rapport");

	rapportL.setAlignmentX(Component.CENTER_ALIGNMENT);
	boutonRapport.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonRapport.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    try{
                        Main.cd.genererRapport();
                    }catch(IOException erreur){
                        System.out.println("erreur");
                    }
		}
        });
	JLabel quitterL = new JLabel("Sauvegarder et quitter");
	JButton boutonQuitter = new JButton("Quitter");

	quitterL.setAlignmentX(Component.CENTER_ALIGNMENT);
	boutonQuitter.setAlignmentX(Component.CENTER_ALIGNMENT);

	boutonQuitter.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    try{
                        Main.cd.sauvegarder();
                        System.exit(0);
                    }catch(IOException erreur){
                        System.out.println("erreur fichier");
                    }
		}
	    });
	JLabel loginL = new JLabel("Retourner au login");
	JButton loginB = new JButton("Retour");

	loginL.setAlignmentX(Component.CENTER_ALIGNMENT);
	loginB.setAlignmentX(Component.CENTER_ALIGNMENT);

	loginB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			MainFrame.login.retourLogin(panel);
		}
        });
	if(type.equals("membre")){
		this.add(membreL);
		JLabel personneCode = new JLabel("Numero de membre: " + personne.code);
		personneCode.setFont(new Font("Verdana", Font.PLAIN, 18));
		personneCode.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel personneQR = new JLabel("code QR: ");
		this.add(personneCode);
		this.add(personneQR);
		this.add(personneQR);
		this.add(modL);
		this.add(modB);
	}else if(type.equals("pro")) {
		this.add(professionnel);
		JLabel personneCode = new JLabel("Numero de Professionnel: " + personne.code);
		personneCode.setFont(new Font("Verdana", Font.PLAIN, 18));
		personneCode.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(personneCode);
		this.add(modL);
		this.add(modB);
	}else {
		this.add(agentL);
	    this.add(membreCL);
	    this.add(boutonCre);
	    //this.add(membreVL);
	    //this.add(boutonVer);
	    this.add(creationPL);
		this.add(boutonCreationP);
		this.add(creationSL);
		this.add(boutonCreationSL);
		this.add(gerantL);
		this.add(rapportL);
		this.add(boutonRapport);
		this.add(consultationPL);
		this.add(consultationPB);
		this.add(membreCoL);
	    this.add(boutonComfirmation);
	    this.add(membreCoL);
	    this.add(membreCoT);
	    this.add(boutonComfirmation);
	}
	this.add(consultationL);
	this.add(consultationB);
	this.add(quitterL);
	this.add(boutonQuitter);
	this.add(loginL);
	this.add(loginB);
    }
    
    public void retour(JPanel panel){
        panel.setVisible(false);
        MainFrame.frame.remove(panel);
        MainFrame.mainMenu.setVisible(true);
    }
}
