import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.imageio.*;

public class VerificationWindow extends JPanel{

    public VerificationWindow(){

    	JPanel panel = this;

    	this.setSize(500,500);
    	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    	//Numero de membre
        JLabel noMembreL = new JLabel("Entrer le numero de membre");
        JTextField noMembreT = new JTextField(7);
        noMembreT.setMaximumSize(new Dimension(70,20));

        JButton comfirmer = new JButton("Comfirmer");
        comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel valide = new JLabel("Pas de nom");

        comfirmer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    String code = noMembreT.getText();
                    if(code.length() == 7){
                        if(Main.cd.contientCode(code)){
                            valide.setText("Valide");
                        }else{
                            valide.setText("Non valide");
                        }
                    }else{
                        valide.setText("Le code doit etre de 7 lettres");
                    }
		}
	    });

        JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);

        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    MainFrame.mainMenu.retour(panel);
		}
	    });

        noMembreT.setAlignmentX(Component.CENTER_ALIGNMENT);
        noMembreL.setAlignmentX(Component.CENTER_ALIGNMENT);
        comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);
        valide.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(noMembreL);
        this.add(noMembreT);
        this.add(comfirmer);
        this.add(valide);
        this.add(retour);
    }

}
