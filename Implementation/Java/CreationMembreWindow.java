import java.awt.*;
import javax.swing.*;
import javax.swing.text.MaskFormatter;

import java.awt.event.*;

public class CreationMembreWindow extends JPanel{

    boolean payer = false;
    
    public CreationMembreWindow(){

    	JPanel panel = this;
    	this.setSize(500,500);
    	this.setBackground(Color.YELLOW);
    	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    	//Numero de membre
        JLabel nomL = new JLabel("Nom: ");
        JTextField nomT = new JTextField(20);
        nomT.setMaximumSize(new Dimension(150,20));
        
        //courriel
        JLabel courrielL = new JLabel("Courriel");
        courrielL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField courrielT = new JTextField(20);
        courrielT.setMaximumSize(new Dimension(150,20));
        //Adresse
        JLabel adresseL = new JLabel("Adresse");
        adresseL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField adresseT = new JTextField(25);
        adresseT.setMaximumSize(new Dimension(150,20));
        //Ville
        JLabel villeL = new JLabel("Ville");
        villeL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JTextField villeT = new JTextField(14);
        villeT.setMaximumSize(new Dimension(150,20));
        //Province
        JLabel provinceL = new JLabel("Province");
        provinceL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JFormattedTextField provinceT = new JFormattedTextField(createFormatter("UU"));
        provinceT.setMaximumSize(new Dimension(150,20));
        //Code postal
        JLabel postalL = new JLabel("Code postal");
        postalL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JFormattedTextField postalT = new JFormattedTextField(createFormatter("U#U #U#"));
        postalT.setMaximumSize(new Dimension(150,20));
        //Paiement
        JLabel paiementL = new JLabel("Paiement: Pas Encore Paye");
        paiementL.setAlignmentX(Component.CENTER_ALIGNMENT);
        JButton paiementB = new JButton("Payer");
        paiementB.setAlignmentX(Component.CENTER_ALIGNMENT);
        paiementB.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    new Paiement();
                    paiementL.setText("Paiement: Accepte");
                    payer = true;
		}
	    });

        JButton comfirmer = new JButton("Comfirmer");
        comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel resultat = new JLabel("Entrez un nom");

        comfirmer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			String email = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					   + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			
                    if(!(nomT.getText().equals("")) && payer && courrielT.getText().matches(email)){
                        String code = Main.cd.addMembre(nomT.getText(),courrielT.getText(),
                        								adresseT.getText(),villeT.getText(),
                        								provinceT.getText(),postalT.getText());
                        resultat.setText("Numero de membre: " + code);
                    }
		}
	    });

        JButton retour = new JButton("Retour");
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);

        retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
                    MainFrame.mainMenu.retour(panel);
		}
	    });

        nomL.setAlignmentX(Component.CENTER_ALIGNMENT);
        nomT.setAlignmentX(Component.CENTER_ALIGNMENT);
        resultat.setAlignmentX(Component.CENTER_ALIGNMENT);
        comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);
        retour.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(nomL);
        this.add(nomT);
        this.add(courrielL);
        this.add(courrielT);
        this.add(adresseL);
        this.add(adresseT);
        this.add(villeL);
        this.add(villeT);
        this.add(provinceL);
        this.add(provinceT);
        this.add(postalL);
        this.add(postalT);
        this.add(paiementL);
        this.add(paiementB);
        this.add(comfirmer);
        this.add(resultat);
        this.add(retour);
    }
    //Cette methode est prise depuis l'internet http://www.java2s.com/Tutorials/Java/Swing_How_to/JFormattedTextField/Create_custom_format_for_Date_Time_value_in_Formattedtextfield.htm
    private MaskFormatter createFormatter(String s) {
        MaskFormatter formatter = null;
        try {
            formatter = new MaskFormatter(s);
        } catch (java.text.ParseException exc) {
            System.err.println("formatter is bad: " + exc.getMessage());
        }
        return formatter;
    }

}
