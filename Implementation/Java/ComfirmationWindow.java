import java.awt.*;

import javax.swing.*;
import java.awt.event.*;
import java.time.*;

public class ComfirmationWindow extends JPanel{
    
    public ComfirmationWindow(){
	
	this.setSize(500,500);
	this.setBackground(Color.ORANGE);
	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	
	JPanel panel = this;

	//Date actuelle
	LocalDate date = java.time.LocalDate.now();  
	//Temps actuel
	LocalTime time = java.time.LocalTime.now();
	JLabel dateT = new JLabel("Date: " + String.valueOf(date));
	JLabel timeT = new JLabel("Heure: " + String.valueOf(time));
	dateT.setAlignmentX(Component.CENTER_ALIGNMENT);
	timeT.setAlignmentX(Component.CENTER_ALIGNMENT);
	//numero des acteurs
	JLabel numeroProT = new JLabel("Numero Professionnel");
	numeroProT.setAlignmentX(Component.CENTER_ALIGNMENT);
	JTextField numeroPro = new JTextField(20);

	JLabel numeroClientT = new JLabel("Numero Client");
	JTextField numeroClient = new JTextField(20);
	numeroClientT.setAlignmentX(Component.CENTER_ALIGNMENT);

	numeroPro.setMaximumSize(new Dimension(1000,20));
    numeroClient.setMaximumSize(
    new Dimension(Integer.MAX_VALUE,
    numeroClient.getPreferredSize().height));
	//Code de service
	JLabel codeServiceT = new JLabel("Code de Service");
	JTextField codeService = new JTextField(20);
	codeServiceT.setAlignmentX(Component.CENTER_ALIGNMENT);
	codeService.setMaximumSize(
    new Dimension(Integer.MAX_VALUE,
    codeService.getPreferredSize().height));
    JLabel commentaireZoneT = new JLabel("Commentaire (facultatif)");
    commentaireZoneT.setAlignmentX(Component.CENTER_ALIGNMENT);
	//Commentaire
	TextArea commentaireZone = new TextArea("", 5, 40);
	commentaireZone.setMaximumSize(new Dimension(300,200));
	JButton comfirmer = new JButton("Comfirmer");
	comfirmer.setAlignmentX(Component.CENTER_ALIGNMENT);
	
	comfirmer.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			if(Main.cd.getProParCode(numeroProT.getText()) != null){
                            if(Main.cd.getMembreParCode(numeroClient.getText()) != null || numeroClient.getText() == ""){
                                if((Main.cd.getMembreParCode(numeroClient.getText()).paye)){
                                    //comfimer
                                }else{
                                    numeroClientT.setText("Membre n'a pas paye");
                                    }
                            }else{
                                numeroClientT.setText("Le membre n'existe pas");
                            }
			}else{
                            numeroProT.setText("Le professionnel n'existe pas");
			}
                        
		}
	    });
	JButton retour = new JButton("Retour");
		retour.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		retour.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e)
		{
			MainFrame.mainMenu.retour(panel);
		}
	    });

	this.add(dateT);
	this.add(timeT);
	this.add(Box.createRigidArea(new Dimension(0, 10)));
	this.add(numeroProT);
	this.add(numeroPro);
	this.add(numeroClientT);
	this.add(numeroClient);
	this.add(codeServiceT);
	this.add(codeService);
	this.add(Box.createRigidArea(new Dimension(0, 10)));
	this.add(commentaireZoneT);
	this.add(commentaireZone);
	this.add(Box.createRigidArea(new Dimension(0, 10)));
	this.add(comfirmer);
	this.add(retour);
	
	}
		
}
