import java.util.*;
import java.io.IOException;
import java.io.*;

public class CentreDeDonne{
    private ArrayList<Membre> listeMembre;
    private ArrayList<Professionnel> listePro;
    private ArrayList<Seance> listeSeance;
    private ArrayList<Inscription> listeInscription;
    
    //La liste des codes de service
    private Map<String, String> codeService = new HashMap<String, String>() {{
        	put("nutritionniste", "598");
        	put("zumba", "883");
    		}};
    
    public String[] getServices() {
    	Object[] listeService = codeService.keySet().toArray();
    	String[] listeServiceS =  Arrays.copyOf(listeService, listeService.length, String[].class);
    	return listeServiceS;
    }
    
	public Inscription ajouterInscription(Seance seance, Membre membre,String date,String heure,String commentaire) {
    	Inscription inscription = new Inscription(seance,membre,date,heure,commentaire);
    	listeInscription.add(inscription);
    	return inscription;
	}
	public void desinscrire(Seance seance, Membre membre) {
		for(int i=0;i<listeInscription.size();i++) {
			if(listeInscription.get(i).seance == seance) {
				for(int j=0;j<listeInscription.get(i).seance.membres.size();j++) {
					if(listeInscription.get(i).seance.membres.get(j) == membre) {
						listeInscription.remove(i);
					}
				}
			}
		}
	}

    public CentreDeDonne(){
        this.listeMembre = new ArrayList<Membre>();
        this.listePro = new ArrayList<Professionnel>();
        this.listeSeance = new ArrayList<Seance>();
        this.listeInscription = new ArrayList<Inscription>();
    }
    
    public String addMembre(String nom, String courriel,
    		String adresse,String ville,String province,
    		String codePostal){

        String code = generateCode("personne");
        System.out.println(code);
        this.listeMembre.add(new Membre(nom, code, courriel,
        					adresse,ville,province,codePostal));

        return code;
    }

    public String addPro(String nom, String courriel,
    					String adresse,String ville,String province,
    					String codePostal){
        String code = generateCode("personne");
        System.out.println(code);
        this.listePro.add(new Professionnel(nom, code, courriel,
        				  adresse,ville,province,codePostal));

        return code;
    }

    public String addSeance(String noPro,
	                        String dateA, String dateD, String dateF, String type,
	                        String heureA, String heureD, String heureF, boolean[] jours,
	                        Integer capaciteMax, double prix, String commentaire){
	
	    String code = generateCodeSeance(noPro,type);
	    System.out.println(code);
	
	    this.listeSeance.add(new Seance(code, noPro, dateA, dateD, dateF,type,
	                                    heureA, heureD, heureF, jours,
	                                    capaciteMax, prix, commentaire));
	    return code;
	}

	public Boolean contientCode(String noMembre){
        System.out.println(noMembre);
        for(Membre membre: this.listeMembre){
            if(membre.code.equals(noMembre)){
                return true;
            }
        }
        return false;
    }

    private String generateCodeSeance(String noPro, String type) {
    	String[] alphabet = {"1","2","3","4","5","6","7","8","9"};
    	String code = "";
    	//Code de service
    	code += codeService.get(type);
    	for(int i=0;i<2;i++){
    		int nombre = (int)(Math.random() * (alphabet.length));
    		code += alphabet[nombre];
    	}
    	//chiffre pro
    	code += noPro.substring(7,9);
    	return code;
    }
    
    private String generateCode(String objet){
        String[] alphabet = {"1","2","3","4","5","6","7","8","9"};
        String code = "";
        int longeur = 9;
    	for(int i=0;i<longeur;i++){
    		int nombre = (int)(Math.random() * (alphabet.length));
    		code += alphabet[nombre];
    	}
        return code;
    }

    public ArrayList<Seance> getListeSeances(){
        return this.listeSeance;
    }

    public ArrayList<Membre> getListeMembre(){
        return this.listeMembre;
    }

    public ArrayList<Professionnel> getListePro(){
        return this.listePro;
    }

    public Membre getMembreParNom(String nom){
        for(int i=0;i<listeMembre.size();i++){
            if(listeMembre.get(i).nom.equals(nom)){
                return listeMembre.get(i);
            }
        }
        return null;
    }

    public Membre getMembreParCode(String code){
        for(int i=0;i<listeMembre.size();i++){
            if(listeMembre.get(i).code.equals(code)){
                return listeMembre.get(i);
            }
        }
        return null;
    }

    public Boolean comfirmerQRMembre(String qr) {
    	Membre membre = getMembreParCode(qr);
    	if(membre != null) {
    		if(membre.paye) {
    			return true;
    		}else {
    			return false;
    		}
    	}else {
    		return false;
    	}
    }
    
    public Professionnel getProParNom(String nom){
        for(int i=0;i<listePro.size();i++){
            if(listePro.get(i).nom.equals(nom)){
                return listePro.get(i);
            }
        }
        return null;
    }

    public Professionnel getProParCode(String code){
        for(int i=0;i<listePro.size();i++){
            if(listePro.get(i).code.equals(code)){
                return listePro.get(i);
            }
        }
        return null;
    }

    public Seance getSeanceParCode(String code){
        for(int i=0;i<listeSeance.size();i++){
            if(listeSeance.get(i).code.equals(code)){
                return listeSeance.get(i);
            }
        }
        return null;
    }

    public Membre comfirmerCourrielMembre(String courriel){
		for(int i=0;i<listeMembre.size();i++) {
			if(listeMembre.get(i).courriel.equals(courriel)){
					return listeMembre.get(i);
			}
		}
		return null;
	}
    
    public Professionnel comfirmerCourrielPro(String courriel){
		for(int i=0;i<listePro.size();i++) {
			if(listePro.get(i).courriel.equals(courriel)){
					return listePro.get(i);
			}
		}
		return null;
	}
    
    public void genererRapport() throws IOException{
        Map<String,Double> dictPro = new HashMap<String,Double>();
        for(int i=0;i<this.listeSeance.size();i++){
            if(!(dictPro.containsKey(this.listeSeance.get(i).noPro))){
                dictPro.put(this.listeSeance.get(i).noPro,(double)0);
            }
            double somme = dictPro.get(this.listeSeance.get(i).noPro) + this.listeSeance.get(i).membres.size()
                * this.listeSeance.get(i).prix * nbrJour(this.listeSeance.get(i).jours);
            dictPro.replace(this.listeSeance.get(i).noPro,somme);
        }
        int totalArgent = 0;
        String output = "";
        for(Map.Entry<String, Double> e: dictPro.entrySet()){
            String nom = getProParCode(e.getKey()).nom;
            totalArgent += e.getValue();
            output += nom + " | " + e.getKey() + " | " + Double.toString(e.getValue()) + "\n";
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter("TEF.txt"));
        writer.write(output);
        writer.close();

        int taille = dictPro.size();

        output += "nombre professionnel: " + taille + " | " + "argent total:" + totalArgent;

        BufferedWriter writer2 = new BufferedWriter(new FileWriter("Rapport.txt"));
        writer2.write(output);
        writer2.close();
        genererRapportPro(dictPro);
        genererRapportMembre();
    }
    public void genererRapportPro(Map<String,Double> liste) throws IOException{
    	for(int i=0;i<listePro.size();i++) {
	    	Professionnel pro = this.listePro.get(i);
			String nomFichier = pro.nom + "_rapport_semaine.txt";
	        BufferedWriter writer = new BufferedWriter(new FileWriter(nomFichier));
	        String output = "";
	        output += "Nom: " + pro.nom + "\n"
	        		+ "Numero: " + pro.code + "\n"
	        		+ "Adresse: " + pro.adresse + "\n"
	        		+ "Ville: " + pro.ville + "\n"
	        		+ "Province: " + pro.province + "\n"
	        		+ "Code postal: " + pro.codePostal + "\n";
	        for(int j=0;j<listeInscription.size();j++) {
	        	Inscription inscription = listeInscription.get(j);
	        	if(inscription.seance.noPro.equals(pro.code)) {
	        	output += "---------------------------\n"
	        			+ "Date recue: " + inscription.date + "\n"
	        			+ "Heure recue: " + inscription.heure + "\n"
	        			+ "Nom du membre: " + inscription.membre.nom + "\n"
	        			+ "Code de la seance: " + inscription.seance.code + "\n";
	        	}
	        }
	        output += "--------------------------\n"
	        		+ "Argent total: " + Double.toString(liste.get(pro.code));
	        writer.write(output);
	        writer.close();
    	}
    	
    }
    
    public void genererRapportMembre() throws IOException{
    	for(int i=0;i<this.listeMembre.size();i++) {
    		Membre membre = this.listeMembre.get(i);
    		String nomFichier = membre.nom + "_rapport_semaine.txt";
            BufferedWriter writer = new BufferedWriter(new FileWriter(nomFichier));
            String output = "";
            output += "Nom: " + membre.nom + "\n"
            		+ "Numero: " + membre.code + "\n"
            		+ "Adresse: " + membre.adresse + "\n"
            		+ "Ville: " + membre.ville + "\n"
            		+ "Province: " + membre.province + "\n"
            		+ "Code postal: " + membre.codePostal + "\n";
            for(int j=0;j<listeInscription.size();j++){
            	Inscription inscription = listeInscription.get(j);
            	System.out.println(inscription.membre.nom);
            	if(inscription.membre == membre) {
            		output += "-----------------------\n"
            				+ "Date du service: " + inscription.date + "\n"
            				+ "Nom du professionnel: " + getProParCode(inscription.seance.noPro).nom + "\n"
            				+ "Nom du service: " + inscription.seance.type + "\n";
            	}
            }
            writer.write(output);
            writer.close();
    	}
	}
    

    public int nbrJour(boolean[] jours){
        int resultat = 0;
        for(int i = 0; i<jours.length;i++){
            if(jours[i]){
                resultat += 1;
            }
        }
        return resultat;
    }

    public void sauvegarder() throws IOException{
        String output = "";
        output += "Membres:\n";
        for(int i=0;i < this.listeMembre.size(); i++){
            output += this.listeMembre.get(i).nom + "," + this.listeMembre.get(i).code + "," + this.listeMembre.get(i).courriel + "," 
            		   + this.listeMembre.get(i).adresse + "," +this.listeMembre.get(i).ville + "," +
            		  this.listeMembre.get(i).province + "," + this.listeMembre.get(i).codePostal + "\n";
        }
        output += "Professionnels:\n";
        
        for(int i=0;i < this.listePro.size(); i++){
            output += this.listePro.get(i).nom + "," + this.listePro.get(i).code + "," + this.listePro.get(i).courriel + ","
            	+ this.listePro.get(i).adresse + "," + this.listePro.get(i).ville + "," +
           		this.listePro.get(i).province + "," + this.listePro.get(i).codePostal + "\n";
        }
        output += "Seances:\n";

        for(int i=0;i < this.listeSeance.size(); i++){

            String reccurence = "";
            for(int j=0;j<7;j++){
                reccurence += Boolean.toString(this.listeSeance.get(i).jours[j]) + ",";
            }
            output += this.listeSeance.get(i).code += ","
                + this.listeSeance.get(i).noPro + ","
                + this.listeSeance.get(i).dateA + ","
                + this.listeSeance.get(i).dateD + ","
                + this.listeSeance.get(i).dateF + ","
                + this.listeSeance.get(i).heureA + ","
                + this.listeSeance.get(i).heureD + ","
                + this.listeSeance.get(i).heureF + ","
                + this.listeSeance.get(i).type + ","
                + reccurence
                + Integer.toString(this.listeSeance.get(i).capaciteMax) + ","
                + Double.toString(this.listeSeance.get(i).prix) + ","
                + this.listeSeance.get(i).commentaire
                + "\n";
        }


        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
        writer.write(output);

        writer.close();
    }

    public void loader(){
        BufferedReader reader;
        try{
            reader = new BufferedReader(new FileReader("output.txt"));
            String line = reader.readLine();
            ArrayList<String> lines = new ArrayList<String>();
            while(line != null){
                line = reader.readLine();
                lines.add(line);
            }
            reader.close();
            int i=0;
            if(lines.size() == 0){
                return;
            }
            while(!(lines.get(i).equals("Professionnels:"))){
                String[] membre = lines.get(i).split(",", 7);
                System.out.println(Arrays.toString(membre));
                listeMembre.add(new Membre(membre[0],membre[1], membre[2],membre[3],membre[4],membre[5],membre[6]));
                i++;
            }
            i++;
            while(!(lines.get(i).equals("Seances:"))){
                String[] pro = lines.get(i).split(",", 7);
                listePro.add(new Professionnel(pro[0],pro[1],pro[2],pro[3],pro[4],pro[5],pro[6]));
                i++;
            }
            i++;
            while(i < lines.size() - 1){
                String[] seance = lines.get(i).split(",");
                System.out.println(Arrays.toString(seance));
                boolean[] jours = new boolean[7];
                for(int k=9,j=0;k<16;k++,j++){
                    jours[j] = Boolean.parseBoolean(seance[k]);
                }
                listeSeance.add(new Seance(seance[0],
                                           seance[1],
                                           seance[2],
                                           seance[3],
                                           seance[4],
                                           seance[8],
                                           seance[5],
                                           seance[6],
                                           seance[7],
                                           jours,
                                           Integer.parseInt(seance[16]),
                                           Double.parseDouble(seance[17]),
                                           seance[10]
                                           ));
                i++;
            }

        }catch(IOException erreur){
            System.out.println("Erreur lecture fichier");
        }
    }

}

class Personne{
    public String nom;
    public String code;
    public String courriel;
    public String adresse;
    public String ville;
    public String province;
    public String codePostal;
    
    Personne(String nom, String code, String courriel,
    		String adresse,String ville,String province,String codePostal){
        this.nom = nom;
        this.code = code;
        this.courriel = courriel;
        this.adresse = adresse;
        this.ville = ville;
        this.province = province;
        this.codePostal = codePostal;
    }
}

class Membre extends Personne{
    boolean paye;
    Membre(String nom, String code, String courriel,
    		String adresse,String ville,String province,String codePostal){
        super(nom,code,courriel,adresse,ville,province,codePostal);
        this.paye = true;
    }
}

class Professionnel extends Personne{
    Professionnel(String nom, String code, String courriel,
    		String adresse,String ville,String province,String codePostal){
        super(nom,code,courriel, adresse,ville,province,codePostal);
    }

}

class Seance{
    public String noPro;
    public String dateA;
    public String dateD;
    public String dateF;
    public String heureA;
    public String heureD;
    public String heureF;
    public String type;
    public Integer capaciteMax;
    public String commentaire;
    public ArrayList<Membre> membres;
    public String code;
    public double prix;
    public boolean[] jours;


    Seance(String code, String noPro,
           String dateA, String dateD, String dateF, String type, String heureA,
           String heureD, String heureF, boolean[] jours, Integer capaciteMax, double prix, String commentaire){

        this.code = code;
        this.noPro = noPro;
        this.dateA = dateA;
        this.dateD = dateD;
        this.dateF = dateF;
        this.type = type;
        this.heureA = heureA;
        this.heureD = heureD;
        this.heureF = heureF;
        this.jours = jours;
        this.capaciteMax = capaciteMax;
        this.commentaire = commentaire;
        this.membres = new ArrayList<Membre>();
        this.prix = prix;
    }
    
    public void ajouterMembreSeance(Membre membre, Seance seance){
        seance.membres.add(membre);
    }
    
}

class Inscription{
	Membre membre;
	Seance seance;
	String heure;
	String date;
	String commentaire;
	
	Inscription(Seance seance,Membre membre, String date,String heure,String commentaire){
	this.seance = seance;
	this.membre = membre;
	this.date = date;
	this.heure = heure;
	this.commentaire = commentaire;
	}
	 
}
