# IFT2255
## Devoir 3
Etudiants:
- Philippe Auclair
- Ngoc-Phong Nguyen

### TO DO:
- [X] Implementation
	- [X] Inclure code Java
- [X] Tests avec JUnit
	- [ ] Screenshot les tests unitaires avec JUnit
- [ ] Generer les Javadocs
- [X] Inclure un programme ANT
- [X] Inclure le programme en fichier JAR
- [ ] Ecrire un manuel utilisateur (~500 mots)
- [X] Inclure les anciens diagrammes mis a jour (cas utilisation, activite, classe, sequence)
- [ ] Inclure graphe BitBucket
- [ ] Faire le rapport en HTML
	- [X] Separer fichiers en 7 dossiers (Exigence, Analyse, Conception, Implementation, Tests, API et Documentation)

